import datetime
import logging
import tweepy
from tweepy import TweepError
import azure.functions as func
import requests as r
from . import keys


def main(mytimer: func.TimerRequest) -> None:
    # Authenticate to Twitter
    auth = tweepy.OAuthHandler(keys.keys["API"], keys.keys["API_SECRET"])
    auth.set_access_token(
        keys.keys["TOKEN"],
        keys.keys["TOKEN_SECRET"],
    )

    # Create API object
    api = tweepy.API(auth)
    tweeted = False
    tries = 0
    while tweeted == False and tries < 10:
        try:
            quote = r.get("https://chainz-rest.azurewebsites.net/quote").json()["quote"]
            alias = r.get("https://chainz-rest.azurewebsites.net/alias").json()["alias"]

            api.update_status(status=f"{quote} - {alias}\n")
            tweeted = True
            tries += 10
        except TweepError:
            tries += 1
            logging.warning(f"{quote} - {alias} - Has already been tweeted")

    utc_timestamp = (
        datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
    )

    if mytimer.past_due:
        logging.info("The timer is past due!")

    logging.info("Python timer trigger function ran at %s", utc_timestamp)

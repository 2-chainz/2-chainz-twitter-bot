You need to add your twitter api keys to a `keys.py` file in this directory with the following format:

```python
keys = {
    "API": "API_KEY_HERE",
    "API_SECRET": "API_SECRET_KEY_HERE",
    "TOKEN": "TOKEN_KEY_HERE",
    "TOKEN_SECRET": "TOKEN_SECRET_KEY_HERE",
}
```

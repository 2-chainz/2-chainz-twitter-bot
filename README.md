# 2 Chainz Twitter Bot

Posts weekly tweets to the [@bot_chainz](https://twitter.com/bot_chainz) twitter account. More information about this project at [2chainz.ansonbiggs.com](https://2chainz.ansonbiggs.com)

## Usage

You need to add your twitter api keys to a `keys.py` file in this directory with the following format:

```python
keys = {
    "API": "API_KEY_HERE",
    "API_SECRET": "API_SECRET_KEY_HERE",
    "TOKEN": "TOKEN_KEY_HERE",
    "TOKEN_SECRET": "TOKEN_SECRET_KEY_HERE",
}
```
